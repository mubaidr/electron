(function() {
    //Setup toolbar
    var remote = require('remote');
    var BrowserWindow = remote.require('browser-window');

    $("#btn_minimize").on("click", function(e) {
        var window = BrowserWindow.getFocusedWindow();
        window.minimize();
    });

    $("#btn_maximize").on("click", function(e) {
        var window = BrowserWindow.getFocusedWindow();
        if (window.isMaximized()) {
            window.unmaximize()
        } else {
            window.maximize();
        }
    });

    $("#btn_close").on("click", function(e) {
        var window = BrowserWindow.getFocusedWindow();
        window.close();
    });

    //Disable page refresh shortcuts
    function disableF5(e) {
        if ((e.which || e.keyCode) == 116) e.preventDefault();
    };
    /* OR jQuery >= 1.7 */
    $(document).on("keydown", disableF5);
})();